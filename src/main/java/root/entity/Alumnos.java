/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Gaby
 */
@Entity
@Table(name = "alumnos")
@NamedQueries({
    @NamedQuery(name = "Alumnos.findAll", query = "SELECT a FROM Alumnos a"),
    @NamedQuery(name = "Alumnos.findByIdRut", query = "SELECT a FROM Alumnos a WHERE a.idRut = :idRut"),
    @NamedQuery(name = "Alumnos.findByAluNombre", query = "SELECT a FROM Alumnos a WHERE a.aluNombre = :aluNombre"),
    @NamedQuery(name = "Alumnos.findByAluApellido1", query = "SELECT a FROM Alumnos a WHERE a.aluApellido1 = :aluApellido1"),
    @NamedQuery(name = "Alumnos.findByAluApellido2", query = "SELECT a FROM Alumnos a WHERE a.aluApellido2 = :aluApellido2"),
    @NamedQuery(name = "Alumnos.findByAluEmail", query = "SELECT a FROM Alumnos a WHERE a.aluEmail = :aluEmail"),
    @NamedQuery(name = "Alumnos.findByAluTelefono", query = "SELECT a FROM Alumnos a WHERE a.aluTelefono = :aluTelefono")})
public class Alumnos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
//    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "id_rut")
    private String idRut;
    @Size(max = 2147483647)
    @Column(name = "alu_nombre")
    private String aluNombre;
    @Size(max = 2147483647)
    @Column(name = "alu_apellido1")
    private String aluApellido1;
    @Size(max = 2147483647)
    @Column(name = "alu_apellido2")
    private String aluApellido2;
       @Size(max = 2147483647)
    @Column(name = "alu_email")
    private String aluEmail;
    @Size(max = 2147483647)
    @Column(name = "alu_telefono")
    private String aluTelefono;

    public Alumnos() {
    }

    public Alumnos(String idRut) {
        this.idRut = idRut;
    }

    public String getIdRut() {
        return idRut;
    }

    public void setIdRut(String idRut) {
        this.idRut = idRut;
    }

    public String getAluNombre() {
        return aluNombre;
    }

    public void setAluNombre(String aluNombre) {
        this.aluNombre = aluNombre;
    }

    public String getAluApellido1() {
        return aluApellido1;
    }

    public void setAluApellido1(String aluApellido1) {
        this.aluApellido1 = aluApellido1;
    }

    public String getAluApellido2() {
        return aluApellido2;
    }

    public void setAluApellido2(String aluApellido2) {
        this.aluApellido2 = aluApellido2;
    }


    

    public String getAluEmail() {
        return aluEmail;
    }

    public void setAluEmail(String aluEmail) {
        this.aluEmail = aluEmail;
    }

    public String getAluTelefono() {
        return aluTelefono;
    }

    public void setAluTelefono(String aluTelefono) {
        this.aluTelefono = aluTelefono;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRut != null ? idRut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumnos)) {
            return false;
        }
        Alumnos other = (Alumnos) object;
        return !((this.idRut == null && other.idRut != null) || (this.idRut != null && !this.idRut.equals(other.idRut)));
    }

    @Override
    public String toString() {
        return "root.entity.Alumnos[ idRut=" + idRut + " ]";
    }

    public void setRut(String rut) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
